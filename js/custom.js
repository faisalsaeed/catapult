// Custom JS 

/*
 * @author: Faisal Saeed
 */
$(function SieveOfEratosthenesCached(n, cache) {


  /*latest news*/
  $('#latest-tweets').owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: false,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 30,
        margin: 30,
        dots: true
      },
      600: {
        items: 1,
        margin: 30,
        stagePadding: 100

      },
      1000: {
        items: 3,
        stagePadding: 100,
        margin: 30

      },
      1645: {
        items: 3,
        stagePadding: 0,
        margin: 30
      }

    }
  })


  /*testimonial*/
  $('#testimonial').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true
      },
      600: {
        items: 1,
        margin: 30,
        stagePadding: 0,
        dots: true

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 30

      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 30
      }

    }
  })

  /*loan carousel*/
  $('#loan-carousel').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true
      },
      600: {
        items: 1,
        margin: 30,
        stagePadding: 0,
        dots: true

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true

      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true
      }

    }
  })


  /*teaser text carousel*/
  $('#teaser-text-carousel').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 0,
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 0,
      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 0,
      }
    }
  })


  /*featured properties */
  $('#featured-properties').owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        dots: true,
      },
      600: {
        items: 2,
        margin: 0,
        stagePadding: 30,
      },
      1000: {
        items: 3,
        stagePadding: 30,
        margin: 0,
      },
      1645: {
        items: 4,
        stagePadding: 30,
        margin: 0,
      }
    }
  })

  /*property slider*/
  $('#property-thumbs-carousel').owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    autoplay: true,
    nav: false,
    dots: false,
    rtl: false,
    thumbs: true,
    thumbsPrerendered: true,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: false
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 20

      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 0
      }

    }
  })


  /*relevant links */
  $('#relevant-links').owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: false
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 20

      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 0
      }

    }
  })


  /*success stories */
  $('#success-stories').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: false,
    dots: true,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 20

      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 0
      }

    }
  })

  /*success stories */
  $('#property-images').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: false
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 3,
        stagePadding: 0,
        margin: 0

      },
      1645: {
        items: 3,
        stagePadding: 0,
        margin: 0
      }

    }
  })

  /*success stories */
  $('#tabs-nav').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    autoplayHoverPause:true,
    responsive: {
      0: {
        items: 2,
        stagePadding: 0,
        margin: 30,
        dots: false
      },
      600: {
        items: 3,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 5,
        stagePadding: 0,
        margin: 0

      },
      1645: {
        items: 8,
        stagePadding: 0,
        margin: 0
      }

    }
  })


  /*premier partnership */
  $('#premier-partnerships-carousel').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: true,
    nav: true,
    dots: true,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true,
        nav: false
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: true,
        nav: false

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 20,
        dots: true,
        nav: false
      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        dots: true,
        nav: false
      }

    }
  })


  /*banking offers  */
  $('#banking-offers-carousel').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: false,
    dots: true,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: true,
        nav: false
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: true,
        nav: false

      },
      1000: {
        items: 1,
        stagePadding: 0,
        margin: 20,
        dots: true,
        nav: false
      },
      1645: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        dots: true,
        nav: false
      }

    }
  })



  /*business accounts */
  $('#business-accounts-carousel').on('initialized.owl.carousel changed.owl.carousel', function (e) {
    if (!e.namespace) {
      return;
    }
    var carousel = e.relatedTarget;
    $('.slider-counter').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
  }).owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30,
        dots: false
      },
      600: {
        items: 1,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 2,
        stagePadding: 100,
        margin: 20

      },
      1645: {
        items: 4,
        stagePadding: 0,
        margin: 0
      }
    }
  });



  /*client carousel*/
  $('.clients-carousel').slick({
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: false,
    vertical: false,
    centerMode: true,
    centerPadding: '0',
    verticalSwiping: false,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


  /*events carousel*/
  $('.events-carousel').slick({
    dots: true,
    infinite: true,
    autoplay: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    vertical: true,
    verticalSwiping: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      }
    ]
  });

  /* calendar*/
  $('.calendar').slick({
    dots: false,
    infinite: false,
    autoplay: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


   /*client carousel*/
   $('.go-getter-testimonials-carousel').slick({
    dots: false,
    infinite: true,
    autoplay: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    vertical: false,
    centerMode: true,
    verticalSwiping: false,
    centerPadding: '0',
    slidesToShow: 3,

    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });



  $(document).ready(function () {

    $("ul.dropdown-menu-mobile [data-toggle='dropdown']").on("click", function (event) {
      event.preventDefault();
      event.stopPropagation();
      $(this).siblings().toggleClass("show");
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu-mobile').first().find('.show').removeClass("show");
      }
      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-submenu .show').removeClass("show");
      });
    });


    /*scroll to top*/
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
      } else {
        $('.scrollup').fadeOut();
      }
    });
    $('.scrollup').click(function () {
      $("html, body").animate({
        scrollTop: 0
      }, 600);
      return false;
    });

    /*add class in top logn form*/
    $(window).on("scroll", function () {
      if ($(window).scrollTop() >= 500) {
        $(".top-form-wrap").addClass("form-scroll");
      } else {
        $(".top-form-wrap").removeClass("form-scroll");
      }
    });



    /*overlay search*/
    $('a[href="#search"]').on("click", function (event) {
      event.preventDefault();
      $("#search").addClass("open-search");
      $('#search > form > input[type="search"]').focus();
    });

    $("#search, #search button.close-search").on("click keyup", function (event) {
      if (
        event.target == this ||
        event.target.className == "close-search" ||
        event.keyCode == 27
      ) {
        $(this).removeClass("open-search");
      }
    });

    /*custom file*/
    $("form").on("change", ".file-upload-field", function () {
      $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    });


    /*range slider*/
    $(function () {
      $('#rangeselector').change(function () {
        $('.range').hide();
        $('#' + $(this).val()).show();
      });
    });


    /*nice select initializer*/
    $(function () {
      $('select').niceSelect();
    });



    $('#search-nav-select').on('change', function () {
      var tabID = $(this).find(":selected").data('toggle');
      $('.search-nav-tabs li a[href="' + tabID + '"]').tab('show');
    });


    /*proceed loan calculator*/
    $("#proceed").on("click", function (e) {
      if ($("#agree").is(':checked')) {
        $('#app-detail').show();
        $('#loan-calculator').hide();
      } else {
        $('#app-detail').hide();
      }
    });

    /*tab scroll down*/
    $('a.nav-link').on('shown.bs.tab', function (e) {
      var href = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(href).offset().top - 50
      }, 'slow');
    });


    $(".toggle-password").click(function () {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    $('.show-hidden-input').click(function () {
      $('.hidden-input').slideToggle("slow");

    });


    $("[name=condition]").change(function () {
      $("#list").toggle($("[name=condition]").index(this) === 3);
    });


    $(".navigation__link").click(function () {
      $('html,body').animate({
          scrollTop: $(".covid-form").offset().top - 200
        },
        'slow');
    });

    $('#datetimepicker').datetimepicker({
      format: 'DD/MM/YYYY'
    });

    $("#email-reminder").click(function () {
      if ($(this).is(":checked")) {
        $("#email").show();
      } else {
        $("#email").hide();
      }
    });









  });

  /*citizenship menu wrapper*/
  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.offcanvas-collapse').toggleClass('open');
    $('body').toggleClass('offcanvas');
  })
  $('.nav-item > a.dropdown-toggle').click(function (e) {


    if ($(this).closest('li.nav-item.dropdown').hasClass('show')) {
      $(this).closest('li.nav-item.dropdown').removeClass("show").find('li').removeClass('show');
    } else {
      $(this).closest('li.nav-item.dropdown').addClass("show")
      $(this).closest('li.nav-item.dropdown').siblings().removeClass('show').find('li').removeClass('show');
    }
  })


  /*change theme mode toggle */
  $('#change-mode').change(function () {
    if ($(this).is(":checked")) {
      $('body').addClass("darkmode");
    } else {
      $('body').removeClass("darkmode");
    }
  });

  /*layout change */
  $('button').on('click', function (e) {
    if ($(this).hasClass('list')) {
      $('#feed-wrapper .wrap').removeClass('grid').addClass('list');
    } else if ($(this).hasClass('grid')) {
      $('#feed-wrapper .wrap').removeClass('list').addClass('grid');
    }
  });

  $(function () {
    $('.layout-buttons .grid').addClass('active');
  });

  $(".grid-list-btn").click(function () {
    $(".grid-list-btn").removeClass("active");
    $(this).addClass("active");
  });

  $(document).click(function (e) {
    if (!$(e.target).is('.panel-body')) {
      $('#setting-collapse').collapse('hide');
    }
  });

});




function increaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('number').value = value;
}